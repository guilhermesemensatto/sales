﻿using Sales.Core.Data;

namespace Sales.Core.Entities
{
    public class Customer : IEntity // cliente
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }        
    }
}