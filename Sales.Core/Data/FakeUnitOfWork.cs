﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Sales.Core.Data
{
    public class FakeUnitOfWork : IUnitOfWork
    {
        List<IEntity> _collectionTracker = new List<IEntity>();
        public void Commit()
        {
            _collectionTracker.Clear();
        }
        
        public void Add<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            _collectionTracker.Add(entity);
            
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity
        {
            return _collectionTracker.OfType<TEntity>().AsQueryable();
        }

        public void Update<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            throw new System.NotImplementedException();
        }

        public void Dispose()
        {
            _collectionTracker = null;
        }

        public int ExecuteSqlCommand(string rawSqlString, params object[] parameters)
        {
            return 1;
        }

        public long GenerateSequence(string seqName)
        {
            return new Random().Next();
        }
    }
}
