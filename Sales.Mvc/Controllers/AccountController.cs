﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Sales.Core.Entities;
using Sales.Mvc.Models;
using Microsoft.Extensions.DependencyInjection;
using Sales.Core.Services.Contracts;
using System;
using Sales.Core;
using Sales.Core.Services;
using Sales.Core.Data;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Sales.Mvc.Controllers
{
    public class AccountController : BaseController
    {
        readonly ILoginService _loginService;

        public AccountController(ILoginService loginService)
        {
            _loginService = loginService;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
                    
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(User u)
        {            
            return TryProcess(() =>
            {
                User user = _loginService.Login(u.Email, u.Password);
                HttpContext.Session.SetString("User", user.Name);
                HttpContext.Session.SetString("ProfileId", user.Profile.ToString());
                HttpContext.Session.SetInt32("UserId", user.Id);
                return RedirectToAction("Index", "Customers");
            });            
        }

        public IActionResult Login()
        {               
            return View();          
        }
        public IActionResult Logout()
        {
            return TryProcess(() =>
            {
                HttpContext.Session.Remove("User");
                HttpContext.Session.Remove("ProfileId");
                HttpContext.Session.Remove("UserId");
                return RedirectToAction("Index", "Home");
            });
        }
    }
}
