﻿using Sales.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.Core.Services.Contracts
{
    public interface ILoginService
    {
        User Login(string email, string password);
    }
}
