﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Sales.Core.Data
{
    internal class SalesUnitOfWork : IUnitOfWork
    {
        readonly DbContext _dbContext;
        public SalesUnitOfWork(SalesDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public long GenerateSequence(string seqName)
        {
            var dofSequence = ExecuteScalar($"SELECT {seqName}.NEXTVAL FROM DUAL");
            return Convert.ToInt64(dofSequence);
        }

        private object ExecuteScalar(string commandText)
        {
            var connection = _dbContext.Database.GetDbConnection();
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = commandText;
            var value = command.ExecuteScalar();
            connection.Close();
            return value;
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity
        {
            return _dbContext.Set<TEntity>().AsNoTracking();
        }

        public void Add<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            _dbContext.Add(entity);
        }

        public void Commit()
        {
            try
            {
                _dbContext.SaveChanges();
            }
            catch (Exception ex) when (ex.InnerException != null)
            {
                throw new ApplicationException(ex.InnerException.Message, ex);
            }
        }

        public void Update<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            _dbContext.Update(entity);
        }

       
        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
