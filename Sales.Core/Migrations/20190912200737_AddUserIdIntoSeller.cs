﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sales.Core.Migrations
{
    public partial class AddUserIdIntoSeller : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                schema: "SALES",
                table: "Seller",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Seller_UserId",
                schema: "SALES",
                table: "Seller",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Seller_User_UserId",
                schema: "SALES",
                table: "Seller",
                column: "UserId",
                principalSchema: "SALES",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Seller_User_UserId",
                schema: "SALES",
                table: "Seller");

            migrationBuilder.DropIndex(
                name: "IX_Seller_UserId",
                schema: "SALES",
                table: "Seller");

            migrationBuilder.DropColumn(
                name: "UserId",
                schema: "SALES",
                table: "Seller");
        }
    }
}
