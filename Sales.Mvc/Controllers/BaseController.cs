﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace Sales.Mvc.Controllers
{
    public class BaseController : Controller
    {
        protected IActionResult TryProcess(Func<IActionResult> process)
        {
            try
            {
                return process();
            }
            catch (ApplicationException ex)
            {
                return this.BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }
    }
}