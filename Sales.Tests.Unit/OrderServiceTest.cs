﻿using Sales.Core.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Sales.Core.Data;
using Sales.Core.Entities;
using Sales.Core.Dtos;

namespace Sales.Tests.Unit
{
    public class OrderServiceTest : UnitTestBase
    {
        readonly IOrderService _orderService;
      
        public OrderServiceTest()
        {
            _orderService = ServiceProvider.GetService<IOrderService>();
        }

        [Fact]
        public void Order_Should_Return_Some_Data() // método_should_esperado
        {
            //Arrange           
            OrderSearchInDto OrderInDto = new OrderSearchInDto()
            {
                SellerName = "Guilherme"
            };

            //Act            
            var orderList = _orderService.Search(OrderInDto);

            //Assert
            Assert.NotNull(orderList);
        }            
        private void SeedOrder()
        {
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(MockOrder());
        }

        private Order MockOrder()
        {
            return new Order()
            {
                Id = 1,
                Number = 504950054,
                SellerId = 2,
                CustomerId = 1
            };
        }

    }
}
