﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Sales.Core.Data;
using Sales.Core.Services;
using Sales.Core.Services.Contracts;

namespace Sales.Core
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSalesUnitOfWork(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<SalesDbContext>();            
            services.AddScoped<IUnitOfWork, SalesUnitOfWork>();
            return services;
        }
        public static IServiceCollection AddFakeUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, FakeUnitOfWork>();
            return services;
        }
        public static IServiceCollection AddServices(this IServiceCollection services)
        {    
            services.AddTransient<ILoginService, LoginService>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<ICustomerService, CustomerService>();
            return services;
        }
    }
}