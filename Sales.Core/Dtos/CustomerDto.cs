﻿using Sales.Core.Data;

namespace Sales.Core.Dtos
{
    public class CustomerDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
        public string SellerName { get; set; }
        
    }
}