﻿using Sales.Core.Data;

namespace Sales.Core.Dtos
{
    public class OrderSearchInDto
    {                                
        public string SellerName { get; set; }
        public string Profile { get; set; }
    }
}