﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Sales.Core;
using Xunit;

namespace Sales.Tests.Unit
{
    public class UnitTestBase
    {
        protected ServiceProvider ServiceProvider { get; set; }
        public UnitTestBase()
        {
            ConfigureServiceCollection();   
        }

        private void ConfigureServiceCollection()
        {
            var serviceCollection = new ServiceCollection();
            AddUnitOfWork(serviceCollection);
            serviceCollection.AddServices();     
            ServiceProvider = serviceCollection.BuildServiceProvider();
        }

        private void AddUnitOfWork(ServiceCollection services)
        {
            var appSettings = new AppSettingsTest();
            if (appSettings.UseMock)
            {
                services.AddFakeUnitOfWork();
            }
            else
            {
                services.AddSalesUnitOfWork(appSettings.ConnectionString);
            }
        }

        protected void Equivalent(object expected, object atual)
        {
            var expectedStr = JsonConvert.SerializeObject(expected);
            var actualStr = JsonConvert.SerializeObject(atual);

            Assert.Equal(expectedStr, actualStr);
        }
    }
}
