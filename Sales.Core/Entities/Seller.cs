﻿using Sales.Core.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.Core.Entities
{
    public class Seller : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
