﻿using Sales.Core.Dtos;
using Sales.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.Core.Services.Contracts
{
    public interface IOrderService
    {
        List<OrderSearchOutDto> Search(OrderSearchInDto orderSearch);
    }
}
