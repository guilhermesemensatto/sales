﻿using Sales.Core.Entities;
using Sales.Core.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.Core.Data
{
    public static class Seed
    {
        public static List<User> GetUsers()
        {
            List<User> users = new List<User>
            {
                new User { Id = 1, Email = "admin@app.com", Password = "password", Name = "Admin", Profile = ProfileEnum.Admin },
                new User { Id = 2, Email = "guilherme@app.com", Password = "senha123", Name = "Guilherme", Profile = ProfileEnum.Seller }
            };
            return users;
        }
        public static List<Seller> GetSellers()
        {
            List<Seller> sellers = new List<Seller>
            {
                new Seller { Id = 1, Name = "Lucas" },
                new Seller { Id = 2, Name = "Guilherme" }
            };
            return sellers;
        }
        public static List<Order> GetOrders()
        {
            List<Order> orders = new List<Order>
            {
                new Order { Id = 1, SellerId = 1, Number = 55893082, CustomerId = 1},
                new Order { Id = 2, SellerId = 2, Number = 55893089, CustomerId = 2},
                new Order { Id = 3, SellerId = 1, Number = 55893065, CustomerId = 3},
                new Order { Id = 4, SellerId = 2, Number = 55893200, CustomerId = 4}
            };
            return orders;
        }
        public static List<Customer> GetCustomers()
        {
            List<Customer> customers = new List<Customer>
            {
                new Customer { Id = 1, Name = "Carlos", City = "Porto Alegre", Phone = "5196665236" },
                new Customer { Id = 2, Name = "Paulo", City = "Porto Alegre", Phone = "51978965236" },
                new Customer { Id = 3, Name = "Maria", City = "Guaíba", Phone = "51898965236" },
                new Customer { Id = 4, Name = "Patricia", City = "Porto Alegre", Phone = "51998965236" }
            };
            return customers;
        }
    }
}
