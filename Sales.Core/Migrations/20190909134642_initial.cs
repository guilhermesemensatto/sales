﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Sales.Core.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "SALES");

            migrationBuilder.CreateTable(
                name: "Customer",
                schema: "SALES",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Seller",
                schema: "SALES",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Seller", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                schema: "SALES",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Email = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Profile = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Order",
                schema: "SALES",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    City = table.Column<string>(nullable: false),
                    Phone = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    SellerId = table.Column<int>(nullable: false),
                    CustomerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Order_Customer_CustomerId",
                        column: x => x.CustomerId,
                        principalSchema: "SALES",
                        principalTable: "Customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Order_Seller_SellerId",
                        column: x => x.SellerId,
                        principalSchema: "SALES",
                        principalTable: "Seller",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "SALES",
                table: "Customer",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Carlos" },
                    { 2, "Paulo" },
                    { 3, "Maria" },
                    { 4, "Patricia" }
                });

            migrationBuilder.InsertData(
                schema: "SALES",
                table: "Seller",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Lucas" },
                    { 2, "Guilherme" }
                });

            migrationBuilder.InsertData(
                schema: "SALES",
                table: "User",
                columns: new[] { "Id", "Email", "Name", "Password", "Profile" },
                values: new object[,]
                {
                    { 1, "admin@app.com", "Admin", "password", 1 },
                    { 2, "guilherme@app.com", "Guilherme", "senha123", 2 }
                });

            migrationBuilder.InsertData(
                schema: "SALES",
                table: "Order",
                columns: new[] { "Id", "City", "CustomerId", "Name", "Phone", "SellerId" },
                values: new object[,]
                {
                    { 1, "Porto Alegre", 1, "João", "5196665236", 1 },
                    { 3, "Guaíba", 3, "Mario", "51898965236", 1 },
                    { 2, "Porto Alegre", 2, "José", "51978965236", 2 },
                    { 4, "Porto Alegre", 4, "Jesus", "51998965236", 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Order_CustomerId",
                schema: "SALES",
                table: "Order",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_SellerId",
                schema: "SALES",
                table: "Order",
                column: "SellerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Order",
                schema: "SALES");

            migrationBuilder.DropTable(
                name: "User",
                schema: "SALES");

            migrationBuilder.DropTable(
                name: "Customer",
                schema: "SALES");

            migrationBuilder.DropTable(
                name: "Seller",
                schema: "SALES");
        }
    }
}
