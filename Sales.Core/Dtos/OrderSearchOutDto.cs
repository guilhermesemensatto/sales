﻿using Sales.Core.Data;
using Sales.Core.Entities;

namespace Sales.Core.Dtos
{
    public class OrderSearchOutDto
    {        
        public OrderSearchOutDto(Order order)
        {
            this.Id = order.Id;
            this.Number = order.Number;           
            this.CustomerName = order.Customer.Name;
            this.SellerId = order.SellerId;
            this.SellerName = order.Seller.Name;
        }
        public int Id { get; set; }
        public int Number { get; set; }
        public string CustomerName { get; set; }        
        public int SellerId { get; set; }    
        public string SellerName { get; set; }
    }
}