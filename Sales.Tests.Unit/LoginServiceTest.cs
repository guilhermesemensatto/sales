using System;
using Xunit;
using Sales.Core;
using Sales.Core.Services;
using Sales.Core.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Sales.Core.Data;
using Sales.Core.Entities;


namespace Sales.Tests.Unit
{
    public class LoginServiceTest : UnitTestBase
    {
        readonly ILoginService _loginService;
        readonly string _password = "password";
        readonly string _email = "admin@app.com";

        public LoginServiceTest()
        {
            _loginService = ServiceProvider.GetService<ILoginService>();
        }

        [Fact]
        public void Login_Should_Return_User_NotNull() // m�todo_should_esperado
        {                        
            //Arrange           
            SeedUser();

            //Act
            var user = _loginService.Login(_email, _password);
            
            //Assert
            Assert.NotNull(user);
        }

        [Fact]
        public void Login_Should_AreEquals() // m�todo_should_esperado
        {
            //Arrange             
            SeedUser();
            var mUser = MockUser();

            //Act
            var user = _loginService.Login(_email, _password);

            //Assert          
            Equivalent(user, mUser);        
        }

        [Fact]
        public void Login_Should_UserNotFound() // m�todo_should_esperado
        {
            //Arrange
            var wrongPassword = "wrong";
            SeedUser();
            var mUser = MockUser();

            
            //Assert 
            Assert.Throws<ApplicationException>(() =>
            {
                //Act
                _loginService.Login(_email, wrongPassword);
            });
              
        }

        [Fact]
        public void Login_Should_PasswordNull() // m�todo_should_esperado
        {
            //Arrange
            string wrongPassword = null;
            SeedUser();
            var mUser = MockUser();

            //Assert 
            Assert.Throws<ArgumentNullException>(() =>
            {
                //Act
                _loginService.Login(_email, wrongPassword);
            });
        }

        private void SeedUser()
        {
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(MockUser());
        }

        private User MockUser()
        {
            return new User()
            {
                Id = 1,
                Name = "Admin",
                Email = "admin@app.com",
                Password = "password",
                Profile = ProfileEnum.Admin
            };
        }
    }
}

