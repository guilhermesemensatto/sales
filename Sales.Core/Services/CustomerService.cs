﻿using Sales.Core.Data;
using Sales.Core.Services.Contracts;
using Sales.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Sales.Core.Dtos;

namespace Sales.Core.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWork _unitOfWork;
      
        public CustomerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;              
        }
        //retorna uma lista de clientes de um vendedor
        public IEnumerable<CustomerDto> GetCustomers(int? sellerId=null)
        {
            var query = _unitOfWork.Query<Order>()
                  .Include(x => x.Customer)
                  .Include(x => x.Seller).AsQueryable();
            if (sellerId.HasValue)
            {
                query = query.Where(e => e.SellerId == sellerId.GetValueOrDefault());
            }
            var customers = query.Select(o => new CustomerDto
            {
                Id = o.Customer.Id,
                Name = o.Customer.Name,
                City = o.Customer.City,
                Phone = o.Customer.Phone,
                SellerName = o.Seller.Name
            });
            return customers.ToList();
        }
        public IEnumerable<CustomerDto> TakeCustomers(int take = 10)
        {
            var query = _unitOfWork.Query<Order>()
                   .Include(x => x.Customer)
                   .Include(x => x.Seller)
                   .Take(take);
            var customers = query.Select(o => new CustomerDto
            {
                Id = o.Customer.Id,
                Name = o.Customer.Name,
                City = o.Customer.City,
                Phone = o.Customer.Phone,
                SellerName = o.Seller.Name      
            });
            return customers.ToList();
        }
    }
}
