﻿using Sales.Core.Data;
using Sales.Core.Services.Contracts;
using Sales.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Sales.Core.Dtos;

namespace Sales.Core.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;

        public OrderService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<OrderSearchOutDto> Search(OrderSearchInDto orderSearch)
        {
            IQueryable<Order> query;
            if (orderSearch.Profile == "Admin")
            {
                query = _unitOfWork.Query<Order>()
                    .Include(x => x.Seller)
                    .Where(x => x.Seller.Id > 0
                    );
            }
            else
            {
                query = _unitOfWork.Query<Order>()
                    .Include(x => x.Seller)
                    .Where(x => x.Seller.Name == orderSearch.SellerName
                    );
            }
            List<Order> order = query.ToList();
            List<OrderSearchOutDto> orderOutList = new List<OrderSearchOutDto>();
            OrderSearchOutDto orderOut;
            foreach (var or in order)
            {
                orderOut = new OrderSearchOutDto(or);
                orderOutList.Add(orderOut);
            }
            return orderOutList;
        }
    }
}
