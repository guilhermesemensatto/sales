﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sales.Core.Data;
using Sales.Core.Entities;
using Sales.Core.Services.Contracts;

namespace Sales.Core.Services
{
    public class LoginService : ILoginService
    {
        private readonly IUnitOfWork _unitOfWork;

        public LoginService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public User Login(string email, string password)
        {
            if (password == null)
            {
                throw new ArgumentNullException(password);
            }

            var query = _unitOfWork.Query<User>()
                .Where(x => x.Email == email
                && x.Password == password);
            var user = query.FirstOrDefault();

            if (user == null)
            {
                var message = "Wrong Email or Password";
                throw new ApplicationException(message);
            }
            return user;
        }
    }
}
