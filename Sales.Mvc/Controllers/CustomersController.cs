﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Sales.Core.Dtos;
using Sales.Core.Entities;
using Sales.Core.Services.Contracts;
using System.Collections.Generic;

namespace Sales.Mvc.Controllers
{
    public class CustomersController : BaseController
    {
        readonly ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public IActionResult Index()
        {
            
            return TryProcess(() =>
            {
                IEnumerable<CustomerDto> customers = null;
                ViewBag.Message = HttpContext.Session.GetString("User");
                var profile = HttpContext.Session.GetString("ProfileId");
                if (HttpContext.Session.GetString("ProfileId") == "Admin")
                {
                    customers = _customerService.GetCustomers();
                    
                }
                else
                {
                    var sellerId = HttpContext.Session.GetInt32("UserId");
                    customers = _customerService.GetCustomers((int)sellerId);
                    
                }
                ViewBag.list = customers;
                return View();
            });


            
        }

    }
}