﻿using Microsoft.EntityFrameworkCore;
using Sales.Core.Entities;
using System.Collections.Generic;
using System.Reflection;

namespace Sales.Core.Data
{
    internal class SalesDbContext : DbContext
    {

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                "Server = (localdb)\\mssqllocaldb; Database=SalesData; Trusted_Connection = True; User Id=AMERICAS\\Guilherme_Semensatto Password=C88L-G64C");
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema("SALES");

            var thisAssembly = Assembly.GetExecutingAssembly();
            builder.ApplyConfigurationsFromAssembly(thisAssembly);
      
            List<Order> ordersList = Seed.GetOrders();
            List<Seller> sellersList = Seed.GetSellers();
            List<User> usersList = Seed.GetUsers();
            List<Customer> customersList = Seed.GetCustomers();

            builder.Entity<User>().HasData(usersList);           
            builder.Entity<Seller>().HasData(sellersList);
            builder.Entity<Customer>().HasData(customersList);
            builder.Entity<Order>().HasData(ordersList);


        }
    }
}
