﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sales.Core.Entities;

namespace Sales.Core.Data
{
    internal class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable("Order");
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id).HasColumnName("Id").IsRequired();            
            builder.Property(c => c.Number).HasColumnName("Number").IsRequired();            
            builder.HasOne(e => e.Seller).WithMany().HasForeignKey(e => e.SellerId);
            builder.HasOne(e => e.Customer).WithMany().HasForeignKey(e => e.CustomerId);
        }
    }
}