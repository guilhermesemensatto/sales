﻿using Sales.Core.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Sales.Core.Data;
using Sales.Core.Entities;
using Sales.Core.Dtos;

namespace Sales.Tests.Unit
{
    public class CustomerServiceTest : UnitTestBase
    {
        readonly ICustomerService _customerService;

        public CustomerServiceTest()
        {
            _customerService = ServiceProvider.GetService<ICustomerService>();
        }

        [Fact]
        public void GetCustomers_Should_Return_Customers_UsingSeller() // método_should_esperado
        {
            //Arrange           
            var sellerId = 3; 
            //SeedCustomer();

            //Act            
            var customers = _customerService.GetCustomers(sellerId);

            //Assert
            Assert.NotNull(customers);
        }
        [Fact]
        public void GetCustomers_Should_Return_AllCustomers() // método_should_esperado
        {
            //Arrange                     

            //Act            
            var customers = _customerService.GetCustomers();

            //Assert
            Assert.NotNull(customers);
        }
        [Fact]
        public void TakeCustomers_Should_Return_N_Customers() // método_should_esperado
        {
            //Arrange                       
            //SeedCustomer();

            //Act              
            var customers = _customerService.TakeCustomers();

            //Assert
            Assert.NotNull(customers);
        }

        private void SeedCustomer()
        {
            var unitOfWork = ServiceProvider.GetService<IUnitOfWork>();
            unitOfWork.Add(MockCustomer());
        }

        private Customer MockCustomer()
        {
            return new Customer()
            {
                Id = 1,
                Name = "Carlos",
                City = "Porto Alegre",
                Phone = "5196665236"
            };
        }
    }
}