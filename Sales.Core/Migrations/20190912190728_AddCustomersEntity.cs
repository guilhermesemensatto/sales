﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sales.Core.Migrations
{
    public partial class AddCustomersEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "City",
                schema: "SALES",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "Name",
                schema: "SALES",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "Phone",
                schema: "SALES",
                table: "Order");

            migrationBuilder.AddColumn<int>(
                name: "Number",
                schema: "SALES",
                table: "Order",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "City",
                schema: "SALES",
                table: "Customer",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                schema: "SALES",
                table: "Customer",
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                schema: "SALES",
                table: "Customer",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "City", "Phone" },
                values: new object[] { "Porto Alegre", "5196665236" });

            migrationBuilder.UpdateData(
                schema: "SALES",
                table: "Customer",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "City", "Phone" },
                values: new object[] { "Porto Alegre", "51978965236" });

            migrationBuilder.UpdateData(
                schema: "SALES",
                table: "Customer",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "City", "Phone" },
                values: new object[] { "Guaíba", "51898965236" });

            migrationBuilder.UpdateData(
                schema: "SALES",
                table: "Customer",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "City", "Phone" },
                values: new object[] { "Porto Alegre", "51998965236" });

            migrationBuilder.UpdateData(
                schema: "SALES",
                table: "Order",
                keyColumn: "Id",
                keyValue: 1,
                column: "Number",
                value: 55893082);

            migrationBuilder.UpdateData(
                schema: "SALES",
                table: "Order",
                keyColumn: "Id",
                keyValue: 2,
                column: "Number",
                value: 55893089);

            migrationBuilder.UpdateData(
                schema: "SALES",
                table: "Order",
                keyColumn: "Id",
                keyValue: 3,
                column: "Number",
                value: 55893065);

            migrationBuilder.UpdateData(
                schema: "SALES",
                table: "Order",
                keyColumn: "Id",
                keyValue: 4,
                column: "Number",
                value: 55893200);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Number",
                schema: "SALES",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "City",
                schema: "SALES",
                table: "Customer");

            migrationBuilder.DropColumn(
                name: "Phone",
                schema: "SALES",
                table: "Customer");

            migrationBuilder.AddColumn<string>(
                name: "City",
                schema: "SALES",
                table: "Order",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                schema: "SALES",
                table: "Order",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                schema: "SALES",
                table: "Order",
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                schema: "SALES",
                table: "Order",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "City", "Name", "Phone" },
                values: new object[] { "Porto Alegre", "João", "5196665236" });

            migrationBuilder.UpdateData(
                schema: "SALES",
                table: "Order",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "City", "Name", "Phone" },
                values: new object[] { "Porto Alegre", "José", "51978965236" });

            migrationBuilder.UpdateData(
                schema: "SALES",
                table: "Order",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "City", "Name", "Phone" },
                values: new object[] { "Guaíba", "Mario", "51898965236" });

            migrationBuilder.UpdateData(
                schema: "SALES",
                table: "Order",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "City", "Name", "Phone" },
                values: new object[] { "Porto Alegre", "Jesus", "51998965236" });
        }
    }
}
