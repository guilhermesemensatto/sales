﻿using System;
using System.Collections.Generic;
using System.Text;
using Sales.Core.Data;
namespace Sales.Core.Entities
{
    public class User : IEntity
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public ProfileEnum Profile { get; set; }            
    }
    public enum ProfileEnum
    {
        None = 0,
        Admin = 1,
        Seller = 2
    }
}
