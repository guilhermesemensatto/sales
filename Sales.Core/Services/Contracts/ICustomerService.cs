﻿using Sales.Core.Dtos;
using Sales.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.Core.Services.Contracts
{
    public interface ICustomerService
    {
        IEnumerable<CustomerDto> GetCustomers(int? sellerId=null);
        IEnumerable<CustomerDto> TakeCustomers(int take = 10);
    }
}
