﻿using Sales.Core.Data;

namespace Sales.Core.Entities
{
    public class Order : IEntity
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public int SellerId { get; set; }
        public Seller Seller { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }      
    }
}