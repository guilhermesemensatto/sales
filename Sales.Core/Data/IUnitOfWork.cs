﻿using System;
using System.Linq;

namespace Sales.Core.Data
{
    public interface IUnitOfWork : IDisposable
    {
        IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity;
        void Add<TEntity>(TEntity entity) where TEntity : class, IEntity;
        void Update<TEntity>(TEntity entity) where TEntity : class, IEntity;
        void Commit();
        long GenerateSequence(string seqName);
     
    }
}
